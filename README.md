## 安装

```bash
yarn add -D typescript ts-node @types/node
```

## 编译运行

### 手动

```bash
npx tsc hello.ts && \
node hello.js
```

### 自动

```bash
ts-node hello.ts
```

如果报错 `Cannot find name 'console'. Do you need to change your target library`：

```json
// tsconfig.json
{
  "compilerOptions": {
    "lib": ["ES2015", "DOM"]
  }
}
```

## 常用类型

### JS 类型

- 原始类型：number, string, boolean, null, undefined, symbol
- 对象类型：object（包括：数组、对象、函数等）

### TS 新增类型

联合、自定义类型（类型别名）、接口、元组、字面量类型、枚举、void、any 等

## 对象类型

TS 中**对象的类型就是在描述对象的结构**（有什么样的属性和方法）

## 接口(interface)和类型别名(type)的对比

- 相同点：都可以给对象指定类型
- 不同点：
  - 接口：只能为对象指定类型
  - 类型别名：不仅可以为对象指定类型，实际上可以为任意类型指定别名

## `keyof` 关键字

接收一个对象类型，生成其名称（可能是字符串或数字）的联合类型。比如： `'name'|'age'|'gender'`

```typescript
function getProp<T, K extends keyof T>(obj: T, key: K) {
  return obj[key];
}

function getProps<T, K extends keyof T>(obj: T, keys: K[]) {
  return keys.map((k) => obj[k]);
}

const p = { name: "yesen", age: 18, gender: "男" };
console.log(getProp(p, "name"));
console.log(getProp(p, "age"));
console.log(getProps(p, ["name", "gender", "age"]));
```
