(() => {
  function id<T>(value: T): T {
    return value;
  }

  const n = id<number>(10);
  const s = id<string>("foobar");

  console.log(n, s);
})();
