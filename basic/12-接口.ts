(() => {
  //   type Person = {
  //     name: string;
  //     age: number;
  //     sayHi(): void;
  //     greet: (name: string) => void;
  //   };

  interface Person {
    name: string;
    age: number;
    sayHi(): void;
    greet: (name: string) => void;
  }

  let zs: Person = {
    name: "张三",
    age: 12,
    sayHi() {
      console.log("张三说");
    },
    greet(name) {
      console.log("来自张三的问候:", name);
    },
  };

  let ls: Person = {
    name: "李四",
    age: 23,
    sayHi() {
      console.log("李四讲");
    },
    greet(name) {
      console.log("李四欢迎你，", name);
    },
  };

  console.log(zs, zs.sayHi(), zs.greet("Yesen"));
  console.log("----------------");
  console.log(ls, ls.sayHi(), ls.greet("Yesen"));
})();
