(() => {
  function id<T>(v: T): T {
    return v;
  }

  let n = id(10);
  const n1 = id(10);

  let s = id("foobar");
  const s1 = id("foobar");

  console.log(n, n1, s, s1);
})();
