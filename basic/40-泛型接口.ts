(() => {
  interface IdFunc<T> {
    id: (v: T) => T;
    ids: () => T[];
  }

  let o: IdFunc<number> = {
    id(v) {
      return v;
    },
    ids() {
      return [1, 2, 3, 4];
    },
  };

  console.log(o.ids());
})();
