(() => {
  interface ILength {
    length: number;
  }

  const len = <T extends ILength>(v: T): number => v.length;

  const sl = len("你好，世界");
  const al = len(["12", "34", "56"]);
  const ol = len({ length: 10, name: "foobar" });

  console.log(sl, al, ol);
})();
