(() => {
  type Keys = "x" | "y" | "z";
  type T1 = { [k in Keys]: number };

  const t: T1 = {
    x: 1,
    y: 2,
    z: 3,
  };

  console.log(t);
})();
