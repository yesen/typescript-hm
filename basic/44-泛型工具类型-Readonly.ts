// Readonly<T>：将 T 所有属性设置为 readonly
(() => {
  interface Props {
    id: number;
    children: number[];
  }

  type ReadonlyProps = Readonly<Props>;

  const a: Props = {
    id: 1,
    children: [1, 2, 3],
  };
  a.id = 123;

  const b: ReadonlyProps = {
    id: 2,
    children: [4, 5, 6],
  };
  // b.id = 456;

  console.log(a, b);
})();
