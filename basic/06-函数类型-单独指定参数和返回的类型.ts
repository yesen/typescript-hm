(() => {
  function add(x: number, y: number): number {
    return x + y;
  }

  const sub = (x: number, y: number): number => {
    return x - y;
  };

  const div = (x: number, y: number): number => (y === 0 ? 0 : x / y);

  const a: number = 123;
  const b: number = 456;

  console.log("add:", add(a, b));
  console.log("sub:", sub(a, b));
  console.log("div:", div(a, b));
})();
