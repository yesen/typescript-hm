(() => {
  interface Point {
    x: number;
    y: number;
  }
  interface Point2D {
    x: number;
    y: number;
  }
  interface Point3D {
    x: number;
    y: number;
    z: number;
  }

  let p1: Point;
  let p2: Point2D = p1;
  let p3: Point3D;
  p2 = p3;
})();
