(() => {
  interface Singable {
    sing(): void;
  }

  class Person implements Singable {
    sing(): void {
      console.log("你是我的小呀小苹果");
    }
  }

  const p = new Person();
  p.sing();
})();
