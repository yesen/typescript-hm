(() => {
  // 语法1
  let numbers: number[] = [1, 3, 5];
  // 语法2
  let strings: Array<string> = ["a", "b", "c"];

  let bs: boolean[] = [false, true];

  console.log(numbers, strings, bs);
})();
