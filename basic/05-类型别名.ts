(() => {
  type CustomeArray = (number | string)[];

  let arr1: CustomeArray = [1, 2, "a", "c"];
  let arr2: CustomeArray = ["x", "y", 7, 9];

  console.log(arr1, arr2);
})();
