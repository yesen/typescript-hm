(() => {
  class Animal {
    private move() {
      console.log("来了来了，它来了");
    }
  }
  class Dog extends Animal {
    public bark() {
      //   this.move();
      console.log("汪汪～");
    }
  }

  const dog = new Dog();
  // dog.move();
  dog.bark();
})();
