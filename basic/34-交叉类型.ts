(() => {
  interface Person {
    name: string;
  }
  interface Contact {
    phone: string;
  }

  // 交叉类型
  type PersonDetail = Person & Contact;

  const p: PersonDetail = {
    name: "Yesen",
    phone: "1234567890",
  };

  console.log(p);
})();
