(() => {
  interface Point2D {
    x: number;
    y: number;
  }
  interface Point3D extends Point2D {
    z: number;
  }

  const p2d: Point2D = { x: 1, y: -345 };
  const p3d: Point3D = { x: 92, y: 83, z: 62 };

  console.log(p2d);
  console.log(p3d);
})();
