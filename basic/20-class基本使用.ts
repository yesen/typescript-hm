(() => {
  class Person {
    age: number;
    gender = "男";
  }

  const p = new Person();
  p.age = 18;
  p.gender = "女";
  console.log(p);
})();
