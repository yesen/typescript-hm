(() => {
  class Point {
    x = 0;
    y = 0;

    scale(n: number): void {
      this.x *= n;
      this.y *= n;
    }
  }

  const p = new Point();
  p.x = 23.12;
  p.y = 492.37;

  console.log(p);

  p.scale(0.258);
  console.log(p);
})();
