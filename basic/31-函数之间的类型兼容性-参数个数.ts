// 参数少的可以赋值给参数多的

(() => {
  type F1 = (x: number) => void;
  type F2 = (a: number, b: number) => void;

  let f1: F1;
  let f2: F2 = f1;
})();
