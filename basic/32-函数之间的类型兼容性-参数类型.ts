// 相同位置的参数，类型要相同(原始类型)或兼容（对象类型）

(() => {
  type F1 = (a: number) => void;
  type F2 = (x: number) => void;
  type F3 = (o: { x: number }) => void;
  type F4 = (o: { x: number; y: number }) => void;

  // 原始类型：类型相同
  let f1: F1;
  let f2: F2 = f1;

  // 对象类型：类型兼容
  let f4: F4;
  let f3: F3 = f4;
  f4 = f3;

  interface P2D {
    x: number;
    y: number;
  }
  interface P3D {
    x: number;
    y: number;
    z: number;
  }
  type F2d = (p: P2D) => void;
  type F3d = (p: P3D) => void;

  let f2d: F2d;
  let f3d: F3d = f2d;
  f2d = f3d;
})();
