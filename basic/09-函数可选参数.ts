(() => {
  function mySlice(start?: number, end?: number): void {
    console.log(`起始索引：${start}，结束索引：${end}`);
  }

  mySlice();
  mySlice(1);
  mySlice(2, 3);

  /*
    起始索引：undefined，结束索引：undefined
    起始索引：1，结束索引：undefined
    起始索引：2，结束索引：3
  */
})();
