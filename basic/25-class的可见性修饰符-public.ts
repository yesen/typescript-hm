(() => {
  class Animal {
    public move() {
      console.log("来了来了，它来了");
    }
  }
  class Dog extends Animal {
    public bark() {
      console.log("汪汪～");
    }
  }

  const dog = new Dog();
  dog.move();
  dog.bark();
})();
