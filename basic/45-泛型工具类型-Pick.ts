// Pick<T,K>：从 T 中选择一组属性来构造新类型

(() => {
  interface Props {
    id: number;
    children: number[];
    title: string;
  }

  type PickProps = Pick<Props, "id" | "title">;

  const a: Props = { id: 123, children: [1, 2, 3], title: "foobar" };
  const b: PickProps = { ...a };

  console.log(a, b);
})();
