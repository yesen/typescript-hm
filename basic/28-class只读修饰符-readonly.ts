(() => {
  class Person {
    readonly age: number; // 只有在构造函数中才能修改它的值

    constructor(age: number) {
      this.age = age;
    }

    // setAge(age: number) {
    //   this.age = age; // 错误
    // }
  }

  class Student extends Person {
    score: number;
    constructor(age: number, score: number) {
      super(age); // ok。其实也是在父类的构造函数修改
      this.score = score;
    }
  }

  const p = new Person(12);
  //   p.age = 24; // 错误
  console.log(p.age);

  const s = new Student(23, 99);
  console.log(s);

  // ----------------

  interface IAnimal {
    readonly name: string;
  }

  const foo: { readonly name: string } = { name: "foobar" };
})();
