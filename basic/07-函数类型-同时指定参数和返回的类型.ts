(() => {
  const add: (x: number, y: number) => number = (x, y) => x + y;

  const a: number = 123;
  const b: number = 456;
  console.log("add:", add(a, b));
})();
