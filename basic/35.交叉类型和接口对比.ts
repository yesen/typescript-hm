(() => {
  interface A {
    fn: (value: number) => string;
  }

  //   // 报错
  //   interface B extends A {
  //     fn: (value: string) => string;
  //   }

  interface B {
    fn: (value: string) => string;
  }

  type C = A & B; // ok ---> fn(value:number|string)=>string

  let foo: C = {
    fn(value: number | string): string {
      return value.toString();
    },
  };

  console.log(foo.fn(123));
  console.log(foo.fn("hello"));
  //   console.log(foo.fn(false)); // 错误
})();
