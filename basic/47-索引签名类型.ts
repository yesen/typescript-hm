(() => {
  interface AnyObject {
    [key: string]: number;
  }

  interface MyArray<T> {
    [n: number]: T;
  }

  const x: AnyObject = {
    a: 1,
    b: 2,
  };
  console.log(x);

  const y: MyArray<number> = [1, 2, 3, 4];
  console.log(y);
})();
