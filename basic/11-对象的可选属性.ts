(() => {
  function myAxios(config: { url: string; method?: string }): void {
    console.log(`url: ${config.url}, method: ${config.method}`);
  }

  myAxios({ url: "foo.bar" });
  myAxios({ url: "foo.bar", method: "POST" });
})();
