(() => {
  type Props = { a: number; b: string; c: boolean };
  type T1 = { [k in keyof Props]: number };

  const t: T1 = {
    a: 1,
    b: 2,
    c: 3,
  };

  console.log(t);
})();
