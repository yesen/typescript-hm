// Record<K, T>：构造一个对象类型，K 为 键，数据类型为 T
(() => {
  type RecordObj = Record<"a" | "b" | "c", number>;
  const x: RecordObj = {
    a: 1,
    b: 2,
    c: 3,
  };

  console.log(x);
})();
