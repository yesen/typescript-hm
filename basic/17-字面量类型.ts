(() => {
  let s1 = "Hello TS"; // string类型
  const s2 = "Hello TS"; // 字面量类型

  let age: 18 = 18;

  function changeDirection(direction: "up" | "down" | "left" | "right"): void {
    console.log(direction);
  }

  changeDirection("down");
})();
