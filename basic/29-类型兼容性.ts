(() => {
  // ts 采用的是鸭子类型

  class Point {
    x: number;
    y: number;
  }
  class Point2D {
    x: number;
    y: number;
  }
  class Point3D {
    x: number;
    y: number;
    z: number;
  }

  const p1: Point = new Point2D();
  const p2: Point = new Point3D();

  const p3: Point2D = new Point3D();
  const p4: Point2D = new Point();
})();
