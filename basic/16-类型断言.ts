(() => {
  //   const aList = document.getElementById("link"); // 自动推断的类型：HTMLElement

  // 方式一
  const aList = document.getElementById("link") as HTMLAnchorElement; // 断言为 HTMLAnchorElement

  // 方式二
  //   const aList = <HTMLAnchorElement>document.getElementById("link");

  aList.href = "https://google.com";
})();
