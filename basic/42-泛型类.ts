(() => {
  class GenericNumber<T> {
    defaultValue: T;
    add: (x: T, y: T) => T;

    constructor(v: T) {
      this.defaultValue = v;
    }
  }

  new GenericNumber<number>(10);
  new GenericNumber(22);
})();
