(() => {
  enum Direction {
    Up,
    Down,
    Left,
    Right,
  }

  function changeDirection(direction: Direction): void {
    console.log(direction);
  }

  changeDirection(Direction.Down);
})();
