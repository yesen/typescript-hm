(() => {
  function getProp<T, K extends keyof T>(obj: T, key: K) {
    return obj[key];
  }

  function getProps<T, K extends keyof T>(obj: T, keys: K[]) {
    return keys.map((k) => obj[k]);
  }

  const p = { name: "yesen", age: 18, gender: "男" };
  console.log(getProp(p, "name"));
  console.log(getProp(p, "age"));
  console.log(getProps(p, ["name", "gender", "age"]));

  const tf = getProp(123, "toFixed");
  console.log(tf);

  const s = getProp("yesen", 0);
  console.log(s);
})();
