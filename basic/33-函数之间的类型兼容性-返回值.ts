(() => {
  type F1 = () => string;
  type F2 = () => string;
  type F3 = () => { name: string };
  type F4 = () => { name: string; age: number };

  let f1: F1;
  let f2: F2 = f1;

  let f4: F4;
  let f3: F3 = f4;
  //   f4 = f3;
})();
