(() => {
  const person: {
    name: string;
    age: number;
    sayHi(): void;
    // greet(name: string): void;
    greet: (name: string) => void;
  } = {
    name: "Yesen",
    age: 18,
    sayHi() {
      console.log("你好，世界");
    },
    greet(name) {
      console.log(`Hello, ${name}`);
    },
  };
  console.log(person);
  person.sayHi();
  person.greet(person.name);
})();
