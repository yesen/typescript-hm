// Partial<T>：用来构造（创建）一个类型，将 T 的所有属性设置为可选
(() => {
  interface Props {
    id: string;
    children: number[];
  }
  type PartialProps = Partial<Props>;

  const a: Props = { id: "123", children: [1, 2, 3, 4] }; // 需要传入属性
  const b: PartialProps = {}; // OK
  console.log(a, b);
})();
